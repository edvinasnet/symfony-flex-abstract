<?php

namespace Symfonyflexab\Controller;

use Symfony\Component\HttpFoundation\Response;

/**
 * Class Controller
 * @package App\Controller
 */
abstract class Controller
{
    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * Controller constructor.
     * @param $twig
     */
    public function __construct(\Twig_Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @param string $view
     * @param array $parameters
     * @param Response|null $response
     * @return Response
     */
    protected function render(string $view, array $parameters = [], Response $response = null): Response
    {
        if (null === $response) {
            $response = new Response();
        }

        $response->setContent(
            $this->twig->render($view, $parameters)
        );

        return $response;
    }

}